__author__ = 'Bogoyavlensky Andrey'
__email__ = 'abogoyavlensky@crystalnix.com'
__copyright__ = 'Copyright 2014, Bogoyavlensky'

from django.contrib.auth.models import AbstractUser


class User(AbstractUser):

    class Meta:
        db_table = 'auth_user'