# -*- coding: utf-8 -*-
"""
Django settings for buildhouse project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
PROJECT_ROOT = os.path.abspath(os.path.join(os.path.dirname(__file__), '..',))
CORE_ROOT = os.path.abspath(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '4@29%&le9^+stx=iet#)5-jg&$4na90zfhgomr_145d^kq8=u)'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    #modern admin interface
    'suit',
    #default django apps
    'django.contrib.auth',
    'django.contrib.admin',
    'django.contrib.sites',
    'django.contrib.comments',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.contenttypes',
    #zinnia
    'tagging',
    'mptt',
    'blog',
    'zinnia_bootstrap',
    'zinnia',
    #apps
    'buildhouse',
    'accounts',
    'main',
    #third-party apps
    'django_extensions',
    'south',
    'gunicorn',
    'easy_thumbnails',
    'pure_pagination',
    'suit_redactor',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'buildhouse.urls'

WSGI_APPLICATION = 'buildhouse.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

# LANGUAGE_CODE = 'en-us'
LANGUAGE_CODE = 'ru'


LANGUAGES = (
   ('ru', 'Русский'),
   # ('en-us', 'English'),
)

LOCALE_PATHS = (
    os.path.join(PROJECT_ROOT, 'locale'),
)

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(PROJECT_ROOT, 'static')

STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(CORE_ROOT, 'static'),
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

#Additional settings

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = '/media/'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'app_namespace.Loader',
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.request',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
    'buildhouse.context_processors.get_settings',
    'zinnia.context_processors.version',  # Optional
)

SUIT_CONFIG = {
    'ADMIN_NAME': 'Строим Дом 2014',
    'MENU_OPEN_FIRST_CHILD': False,
}

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOG_FILE = '{0}/buildhouse.log'.format(PROJECT_ROOT)
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(name)s %(process)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True,
        },
        'logfile': {
            'level': 'DEBUG',
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': LOG_FILE,
            'formatter': 'verbose',
        }
    },
    'loggers': {
        '': {
            'handlers': ['logfile'],
            'propagate': True,
            'level': 'DEBUG',
        },
        'django.request': {
            'handlers': ['mail_admins', 'logfile'],
            'propagate': True,
            'level': 'ERROR',
        },
        'django.db.backends': {
            'handlers': ['null', ],
            'propagate': False,
            'level': 'DEBUG',
        },
        'factory': {
            'handlers': ['null'],
            'propagate': False,
            'level': 'DEBUG',
        },
    }
}

#extend user model
AUTH_USER_MODEL = 'accounts.User'

# Login/Registration settings
LOGIN_URL = '/accounts/login'
LOGIN_REDIRECT_URL = '/'

SITE_ID = 2

# Easy-thumbnails settings
THUMBNAIL_PRESERVE_EXTENSIONS = True
THUMBNAIL_ALIASES = {
    '': {
        'preview': {'size': (700, 400), 'crop': 'scale', 'quality': 95},
    },
    'main.ReadyObject.preview_image': {
        'small': {'size': (500, 300), 'crop': 'scale', 'quality': 95},
        'middle': {'size': (700, 400), 'crop': 'scale', 'quality': 95},
        'large': {'size': (750, 500), 'crop': 'scale', 'quality': 95},
    },
    'main.BuildingSet.preview_image': {
        'small': {'size': (500, 300), 'crop': 'scale', 'quality': 95},
        'middle': {'size': (700, 400), 'crop': 'scale', 'quality': 95},
        'large': {'size': (750, 500), 'crop': 'scale', 'quality': 95},
    },
    'main.Furniture.preview_image': {
        'small': {'size': (500, 300), 'crop': 'scale', 'quality': 95},
        'middle': {'size': (700, 400), 'crop': 'scale', 'quality': 95},
        'large': {'size': (750, 500), 'crop': 'scale', 'quality': 95},
    },
    'main.Material.preview_image': {
        'small': {'size': (500, 300), 'crop': 'scale', 'quality': 95},
        'middle': {'size': (700, 400), 'crop': 'scale', 'quality': 95},
        'large': {'size': (750, 500), 'crop': 'scale', 'quality': 95},
    },
    'main.SpecialEquipment.preview_image': {
        'small': {'size': (500, 300), 'crop': 'scale', 'quality': 95},
        'middle': {'size': (700, 400), 'crop': 'scale', 'quality': 95},
        'large': {'size': (750, 500), 'crop': 'scale', 'quality': 95},
    },
}

SOUTH_MIGRATION_MODULES = {
    'easy_thumbnails': 'easy_thumbnails.south_migrations',
}

#pure-pagination
PAGINATION_SETTINGS = {
    'PAGE_RANGE_DISPLAYED': 5,
    'MARGIN_PAGES_DISPLAYED': 2,
}

#offer types
READY = 'ready'
BUILDING_SET = 'buildingset'
FURNITURE = 'furniture'
MATERIAL = 'material'
SPECIAL_EQUIPMENT = 'specialequipment'

#default object type for offer instances
DEFAULT_OBJECT_TYPE = 'all'

try:
    from local_settings import *
except:
    pass

