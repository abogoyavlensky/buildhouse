from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^blog/', include('blog.urls')),
    # url(r'^accounts/', include('accounts.urls')),
    # url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('main.urls')),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

handler403 = 'buildhouse.views.handler403'
handler404 = 'buildhouse.views.handler404'
handler500 = 'buildhouse.views.handler500'