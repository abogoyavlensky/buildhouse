__author__ = 'Bogoyavlensky Andrey'
__email__ = 'abogoyavlensky@crystalnix.com'
__copyright__ = 'Copyright 2014, Bogoyavlensky'

from django.conf.urls import patterns, include, url


urlpatterns = patterns('blog.views',
    url(r'^', include('zinnia.urls')),
)
