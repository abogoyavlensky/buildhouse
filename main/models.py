__author__ = 'Bogoyavlensky Andrey'
__email__ = 'abogoyavlensky@crystalnix.com'
__copyright__ = 'Copyright 2014, Bogoyavlensky'

from django.conf import settings

from django.db import models
from django.dispatch import receiver
from easy_thumbnails.signals import saved_file
from easy_thumbnails.files import generate_all_aliases
from django.utils.translation import ugettext_lazy as _
from django_extensions.db.models import TimeStampedModel
from django.db.models.fields.files import ImageFieldFile
from easy_thumbnails.fields import ThumbnailerImageField


class CommonMethodsMixin(object):
    def get_object_type(self):
        if hasattr(self, 'object_type'):
            return self.object_type
        return settings.DEFAULT_OBJECT_TYPE


class ReadyObject(CommonMethodsMixin, TimeStampedModel):
    BATH = 'bath'
    STOCKROOM = 'stockroom'
    PLAY_HOUSE = 'playhouse'
    OTHER = 'other'

    TYPES = (
        (BATH, _('Bath')),
        (STOCKROOM, _('Stockroom')),
        (PLAY_HOUSE, _('Play House')),
        (OTHER, _('Other Type')),
    )

    name = models.CharField(_('name'), max_length=225)
    preview_image = ThumbnailerImageField(_('preview image'),
                                          upload_to='ready_object_images',
                                          blank=True, null=True)
    full_description = models.TextField(_('full description'),
                                        max_length=255, blank=True, default='')
    short_description = models.TextField(_('short description'),
                                         max_length=255, blank=True, default='')

    object_type = models.CharField(choices=TYPES, default=OTHER, max_length=255)

    @property
    def get_name(self):
        return settings.READY

    class Meta:
        ordering = ['created']
        verbose_name = _('ready object')
        verbose_name_plural = _('ready objects')

    def __unicode__(self):
        return self.name


class BuildingSet(CommonMethodsMixin, TimeStampedModel):
    name = models.CharField(_('name'), max_length=225)
    preview_image = ThumbnailerImageField(_('preview image'),
                                          upload_to='building_set_images',
                                          blank=True, null=True)
    full_description = models.TextField(_('full description'),
                                        max_length=255, blank=True, default='')
    short_description = models.TextField(_('short description'),
                                         max_length=255, blank=True, default='')

    @property
    def get_name(self):
        return settings.BUILDING_SET

    class Meta:
        ordering = ['created']
        verbose_name = _('building set')
        verbose_name_plural = _('building sets')

    def __unicode__(self):
        return self.name


class Furniture(CommonMethodsMixin, TimeStampedModel):
    name = models.CharField(_('name'), max_length=225)
    preview_image = ThumbnailerImageField(_('preview image'),
                                          upload_to='furniture_images',
                                          blank=True, null=True)
    full_description = models.TextField(_('full description'),
                                        max_length=255, blank=True, default='')
    short_description = models.TextField(_('short description'),
                                         max_length=255, blank=True, default='')

    @property
    def get_name(self):
        return settings.FURNITURE

    class Meta:
        ordering = ['created']
        verbose_name = _('furniture')
        verbose_name_plural = _('furnitures')

    def __unicode__(self):
        return self.name


class Material(CommonMethodsMixin, TimeStampedModel):
    INTERNAL = 'internal'
    OUTSIDE = 'outside'
    DOORS_AND_WINDOWS = 'doorsandwindows'
    BASE = 'base'
    OTHER = 'other'

    TYPES = (
        (INTERNAL, _('Internal')),
        (OUTSIDE, _('Outside')),
        (DOORS_AND_WINDOWS, _('Doors and Windows')),
        (BASE, _('Base')),
        (OTHER, _('Other Type')),
    )

    name = models.CharField(_('name'), max_length=225)
    preview_image = ThumbnailerImageField(_('preview image'),
                                          upload_to='material_images',
                                          blank=True, null=True)
    full_description = models.TextField(_('full description'),
                                        max_length=255, blank=True, default='')
    short_description = models.TextField(_('short description'),
                                         max_length=255, blank=True, default='')

    object_type = models.CharField(choices=TYPES, default=OTHER, max_length=255)

    @property
    def get_name(self):
        return settings.MATERIAL

    class Meta:
        ordering = ['created']
        verbose_name = _('material')
        verbose_name_plural = _('materials')

    def __unicode__(self):
        return self.name


class SpecialEquipment(CommonMethodsMixin, TimeStampedModel):
    name = models.CharField(_('name'), max_length=225)
    preview_image = ThumbnailerImageField(_('preview image'),
                                          upload_to='special_equipment_images',
                                          blank=True, null=True)
    full_description = models.TextField(_('full description'),
                                        max_length=255, blank=True, default='')
    short_description = models.TextField(_('short description'),
                                         max_length=255, blank=True, default='')

    @property
    def get_name(self):
        return settings.SPECIAL_EQUIPMENT

    class Meta:
        ordering = ['created']
        verbose_name = _('special equipment')
        verbose_name_plural = _('special equipment')

    def __unicode__(self):
        return self.name


#SIGNALS
@receiver(saved_file)
def generate_thumbnails_async(sender, fieldfile, **kwargs):
    pk = fieldfile.instance.pk
    field = fieldfile.field.name
    instance = sender._default_manager.get(pk=pk)
    fieldfile = getattr(instance, field, None)
    if fieldfile and isinstance(fieldfile, ImageFieldFile):
        generate_all_aliases(fieldfile, include_global=True)