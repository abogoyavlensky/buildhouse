__author__ = 'Bogoyavlensky Andrey'
__email__ = 'abogoyavlensky@crystalnix.com'
__copyright__ = 'Copyright 2014, Bogoyavlensky'

from .models import ReadyObject, BuildingSet, Furniture, \
    Material, SpecialEquipment

from django.contrib import admin
from django.forms import ModelForm
from suit_redactor.widgets import RedactorWidget


class PageForm(ModelForm):
    class Meta:
        widgets = {
            'full_description': RedactorWidget(),
            'short_description': RedactorWidget()
        }


class ReadyObjectAdmin(admin.ModelAdmin):
    list_display = ['name', 'created']
    form = PageForm


class BuildingSetAdmin(admin.ModelAdmin):
    list_display = ['name', 'created']
    form = PageForm


class FurnitureAdmin(admin.ModelAdmin):
    list_display = ['name', 'created']
    form = PageForm


class MaterialAdmin(admin.ModelAdmin):
    list_display = ['name', 'created']
    form = PageForm


class SpecialEquipmentAdmin(admin.ModelAdmin):
    list_display = ['name', 'created']
    form = PageForm

admin.site.register(ReadyObject, ReadyObjectAdmin)
admin.site.register(BuildingSet, BuildingSetAdmin)
admin.site.register(Furniture, FurnitureAdmin)
admin.site.register(Material, MaterialAdmin)
admin.site.register(SpecialEquipment, SpecialEquipmentAdmin)
